## 簡易Twitterに埋め込んだバグリスト

- データベース接続
  - DBUtil.java
    - MySQLのpassword未記述

    ```java
    private static final String PASSWORD = "";
    ```

- Top画面
  - TopServlet.java
    - sessionのログインユーザー情報を使ったつぶやくボタンの表示条件が逆

    ```java
    boolean isShowMessageForm = false;
    User user = (User) request.getSession().getAttribute("loginUser");
    if (user == null) {
      isShowMessageForm = true;
    }
    ```
  - top.jsp
    - つぶやくボタンがハイパーリンク

    ```html
    <a href="./">つぶやく</a>（140文字まで）
    ```

- 新規登録画面
  - SignUpServlet.java
    - 名前を20文字以内で新規登録しようとしているのにエラー
    ```java
    if (!StringUtils.isEmpty(name) && (20 > name.length())) {
        errorMessages.add("名前は20文字以下で入力してください");
      }
    ```
  - UserDao.java
    - SQLエラー(カンマ多すぎ)
    ```SQL
    sql.append("INSERT INTO users ( ");
    sql.append("	account, ");
    sql.append("	name, ");
    sql.append("	email, ");
    sql.append("	password, ");
    sql.append("	description, ");
    sql.append("	created_date, ");
    sql.append("	updated_date, ");
    sql.append(") VALUES ( ");
    sql.append("	?, ");					// account
    sql.append("	?, ");					// name
    sql.append("	?, ");					// email
    sql.append("	?, ");					// password
    sql.append("	?, ");					// description
    sql.append("	CURRENT_TIMESTAMP, ");	// created_date
    sql.append("	CURRENT_TIMESTAMP ");	 // updated_date
sql.append(")");
```
  - SignUpServlet.java
  - signup.jsp
    - getParameterのミス(Servletでは"Account"、jspでは"account")

- ユーザー編集画面
  - setting.jsp
    - 更新押下時画面遷移しない(form method ="get")
  - UserDao.java
    - updateが上手くいかない(SQLエラー、スペース足らず)
    ```SQL
    sql.append("UPDATE users SET ");
    sql.append("	account = ?, ");
    sql.append("	name = ?, ");
    sql.append("	email = ?, ");
    sql.append("	password = ?, ");
    sql.append("	description = ?, ");
    sql.append("	updated_date = CURRENT_TIMESTAMP");
    sql.append("WHERE id = ?");

## 保留
◆ログイン
パスワード取ってない(request.getParameter()していない)
